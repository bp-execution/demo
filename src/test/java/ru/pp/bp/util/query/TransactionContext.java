package ru.pp.bp.util.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.function.Supplier;

/**
 * Базовая реализация контекста транзакции. Суперкласс для реализации запросов.
 *
 * <p>Часть работы исполняется на стороне клиента данного контекста, но в рамках транзакции
 * в блоке передаваемого сюда объекта типа {@link Supplier}.
 * <p>Другая часть работы исполняется в методе {@link #execute(Supplier, EntityManager)},
 * специфика которого должна быть уточнена в реализации подкласса.
 * <p>Также допустимы реализации в виде анонимных классов.
 * В этом случае придется добавить еще один переопределяемый метод получения {@link EntityManagerFactory}
 * или передавать его в конструктор, если класс конструируется налету.
 */
@Slf4j
public abstract class TransactionContext<R, P> implements Transaction<R, P> {
    @Autowired
    private EntityManagerFactory factory;

    /** Общий уровень изоляции. */
    @Override
    public R execute(Supplier<P> supplier) {
        final EntityManager manager = factory.createEntityManager();
        final EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        try {
            R result = execute(supplier, manager);
            transaction.commit();
            return result;
        } catch (Exception e) {
            log.error("Transaction rollback due to error", e);
            transaction.rollback();
            throw e;
        } finally {
            manager.close();
        }
    }

    /** Специфичная часть работы. */
    protected abstract R execute(Supplier<P> supplier, EntityManager manager);
}
