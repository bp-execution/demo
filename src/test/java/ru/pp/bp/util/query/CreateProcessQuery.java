package ru.pp.bp.util.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.service.TimeFactory;

import javax.persistence.EntityManager;
import java.util.UUID;
import java.util.function.Supplier;

/** Реализация запроса на INSERT */
@Component
public class CreateProcessQuery extends TransactionContext<Process, UUID> {
    @Autowired
    private TimeFactory timeFactory;

    @Override
    protected Process execute(Supplier<UUID> supplier, EntityManager manager) {
        Process process = new Process(supplier.get());
        process.setStatus(Status.created);
        process.setTime(timeFactory.currentTimestamp());
        manager.persist(process);
        return process;
    }
};
