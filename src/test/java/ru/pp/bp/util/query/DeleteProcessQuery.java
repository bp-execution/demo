package ru.pp.bp.util.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;

import javax.persistence.EntityManager;
import java.util.function.Supplier;

/** Для симметрии. :) Ну и для тестов. */
@Component
public class DeleteProcessQuery extends TransactionContext<Process, Process> {
    @Autowired
    private GetProcessQuery query;

    @Override
    protected Process execute(Supplier<Process> supplier, EntityManager manager) {
        Process process = supplier.get();
        // java.lang.IllegalArgumentException: Removing a detached instance Process#97f1a581-3331-4e48-af3f-016df1ceee10
        Process attached = manager.merge(process);
        manager.remove(attached);
        return attached;
    }
};