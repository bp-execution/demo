package ru.pp.bp.util.query;

import java.util.function.Supplier;

/** Контекст исполнения транзакций. */
public interface Transaction<R, P> {

    R execute(Supplier<P> supplier);
}
