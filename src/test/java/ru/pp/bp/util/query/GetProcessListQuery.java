package ru.pp.bp.util.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Supplier;

/**
 * Запрос выборки Процессов по статусу.
 *
 * <p>Это тоже работает
 * <p>     import javax.persistence.Query;
 * <p>     import javax.persistence.criteria.CriteriaBuilder;
 * <p>     import javax.persistence.criteria.CriteriaQuery;
 * <p>     import javax.persistence.criteria.Path;
 * <p>     import javax.persistence.criteria.Root;
 * <p>
 * <p>     CriteriaBuilder builder = manager.getCriteriaBuilder();
 * <p>     CriteriaQuery<Process> criteria = builder.createQuery(Process.class);
 * <p>     Root<Process> root = criteria.from(Process.class);
 * <p>     criteria.where(builder.equal(root.get("status"), supplier.get()));
 * <p>     criteria.orderBy(builder.asc(root.get("time")));
 * <p>     return manager.createQuery(criteria.select(root)).getResultList();
 * <p>
 * <p>Это тоже работает, но т.к. здесь нет определения PostgreSQLEnumType, то сами выполняем toString
 * <p>     import org.springframework.beans.factory.annotation.Value;
 * <p>     @Value("${native.start.query}")
 * <p>     private String sqlQuery;
 * <p>
 * <p>     return manager.createNativeQuery(sqlQuery, Process.class).
 * <p>             setParameter("status", supplier.get().toString()).getResultList();
 */
@Slf4j
@Component
public class GetProcessListQuery extends TransactionContext<List<Process>, Status> {
    /** Оставлен JPQL, не как самый быстрый, а как наиболее компактный и соответствующий JPA. */
    @Override
    protected List<Process> execute(Supplier<Status> supplier, EntityManager manager) {
        return manager.createQuery("from Process where status = :s", Process.class).
                setParameter("s", supplier.get()).getResultList();
    }
}