package ru.pp.bp.util.query;

import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;

import javax.persistence.EntityManager;
import java.util.UUID;
import java.util.function.Supplier;

@Component
public class GetProcessQuery extends TransactionContext<Process, UUID> {
    @Override
    protected Process execute(Supplier<UUID> supplier, EntityManager manager) {
        return manager.find(Process.class, supplier.get());
    }
};