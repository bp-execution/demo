package ru.pp.bp.util.query;

import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;

import javax.persistence.EntityManager;
import java.util.function.Supplier;

@Component
public class UpdateProcessQuery extends TransactionContext<Boolean, Process> {
    @Override
    protected Boolean execute(Supplier<Process> supplier, EntityManager manager) {
        return manager.merge(supplier.get()) != null;
    }
}