package ru.pp.bp.util.concurrent;

import org.junit.rules.MethodRule;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@link MethodRule} будет перехватывать все тестовые вызовы.
 * {@link ConcurrentRule} проверяет, есть ли у перехваченного метода тестирования аннотация {@link Concurrent}
 * Если да, то запускается запрошенное количество потоков и метод тестирования выполняется для каждого потока.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Concurrent {
    int value() default 10;
}
