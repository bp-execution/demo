package ru.pp.bp.util.concurrent;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Запускает тест в нескольких потоках в соответствии со значением переданным в параметре
 * аннотации {@link Concurrent}
 * Перехватывает исключения. Если исключение возникло в одном из потоков, - весь тест останавливается.
 */
public final class ConcurrentRule implements MethodRule {
    @Override
    public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, final Object o) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Concurrent concurrent = frameworkMethod.getAnnotation(Concurrent.class);
                if (concurrent == null)
                    statement.evaluate();
                else {
                    // Просто для передачи в ExecutorCompletionService
                    Executor executor = new Executor() {
                        final String name = frameworkMethod.getName();
                        int count = 0;

                        @Override
                        public void execute(@NotNull Runnable command) {
                            new Thread(command, name + "-Thread-" + count++).start();
                        }
                    };
                    // По мере завершения потоков мы сможем получать результаты их работы или исключения
                    CompletionService<Void> completionService = new ExecutorCompletionService<>(executor);
                    // Для одновременного запуска всех потоков
                    final CountDownLatch go = new CountDownLatch(1);
                    int numThreads = concurrent.value();
                    // Сохраняем для возможности останова
                    List<Future<Void>> futures = new ArrayList<>();
                    for (int i = 0; i < numThreads; i++) {
                        Future<Void> future = completionService.submit(() -> {
                            try {
                                go.await();
                                statement.evaluate();
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            } catch (Throwable throwable) {
                                if (throwable instanceof Exception)
                                    throw (Exception) throwable;
                                if (throwable instanceof Error)
                                    throw (Error) throwable;
                                // Кто-то напрямую засабклассил Throwable
                                RuntimeException ex = new RuntimeException(throwable.getMessage(), throwable);
                                ex.setStackTrace(throwable.getStackTrace());
                                throw ex;
                            }
                            return null;
                        });
                        futures.add(future);
                    }
                    // Запускаем всю конструкцию
                    go.countDown();
                    for (int i = 0; i < numThreads; i++) {
                        try {
                            // Собираем результаты. Здесь Future.get() может выбросить исключение, которое
                            // мы перехватывали выше
                            completionService.take().get();
                        } catch (ExecutionException e) {
                            // Сохраняем первое попавшееся исключение
                            Throwable throwable = e.getCause();
                            // Останавливаем все потоки. (Не будем этого делать - в коносоль
                            // спамятся стеки InterruptedException
                            // futures.forEach(future -> future.cancel(true));
                            throw throwable;
                        }
                    }
                }
            }
        };
    }
}
