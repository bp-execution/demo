package ru.pp.bp.impl.uuid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OptimisticUUIDFactoryTest {
    @Autowired
    private OptimisticUUIDFactory factory;

    /** "Хвосты" должны совпадать. */
    @Test
    public void testRandomUUID() {
        UUID u1 = factory.randomUUID();
        UUID u2 = factory.randomUUID();
        assertThat(u1.toString().substring(24)).isEqualTo(u2.toString().substring(24));
    }

    @Test
    public void testFromString() {
        assertThat(factory.fromString(null)).isNull();
        assertThat(factory.fromString("")).isNull();
        assertThat(factory.fromString("29169f15-a2114894af0d-016ddc4cd9a8")).isNull();
        assertThat(factory.fromString("29169f15-a211-4894-af0d-016ddc4cd9a8")).isNotNull();
    }
}