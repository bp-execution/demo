package ru.pp.bp.impl.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ISO8601TimeFactoryTest {
    @Autowired
    private ISO8601TimeFactory factory;

    @Test
    public void testToString() {
        String timeAsString = "2019-10-19T13:00:24.472+03:00";
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        long time = ZonedDateTime.from(formatter.parse(timeAsString)).toInstant().toEpochMilli();
        assertThat(factory.toString(new Timestamp(time))).isEqualTo("2019-10-19T13:00:24.472+03:00");
    }
}