package ru.pp.bp.impl.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.pp.bp.framework.data.Process;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource(locations = {"classpath:test.properties"})
@ContextConfiguration(classes = AnnotationValueSetupTest.Config.class)
public class AnnotationValueSetupTest {

    /** Максимальная емкость буфера */
    @Value("#{T(java.lang.Integer).parseInt(${cache.capacity} ?: '100000')}")
    private int capacity;

    /** Ключ в атрибутах запроса для трансфера колбэка. */
    @Value("${ru.pp.bp.callback.key}")
    private String processRunnerKey;

    /** Время симуляции исполнения {@link Process Процессов}. По-умолчанию 2 минуты. */
    @Value("#{T(java.lang.Long).parseLong(${process.simulation.time} ?: '120000')}")
    private Long simulationTime;

    /** Размер пула потоков, выполняющих запуск {@link Process Процессов} */
    @Value("#{T(java.lang.Integer).parseInt(${thread.pool.running.size} ?: '10')}")
    private int consumersCount;

    @Value("${native.start.query}")
    private String sqlQuery;

    @Value("#{T(java.lang.Integer).parseInt(${thread.pool.finished.size} ?: '10')}")
    private Integer finishedSize;

    @Test
    public void testValuesSetup() {
        assertThat(capacity).isEqualTo(999);
        assertThat(processRunnerKey).isEqualTo("ru.pp.bp.callback.key");
        assertThat(simulationTime).isEqualTo(4000);
        assertThat(consumersCount).isEqualTo(9);
        assertThat(finishedSize).isEqualTo(9);
        assertThat(sqlQuery).isEqualTo("SELECT p.id, p.time, p.status FROM process p WHERE p.status LIKE :status ORDER BY p.time");
    }

    @Configuration
    static class Config {
        @Bean
        public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}
