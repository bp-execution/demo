package ru.pp.bp.impl.service;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.service.ProcessService;
import ru.pp.bp.impl.uuid.OptimisticUUIDFactory;
import ru.pp.bp.util.concurrent.Concurrent;
import ru.pp.bp.util.concurrent.ConcurrentRule;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static io.restassured.RestAssured.*;
import static org.hamcrest.core.StringContains.containsString;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = {"classpath:test.properties"})
public class DefaultProcessServiceTest {
    @Autowired
    private OptimisticUUIDFactory factory;
    /** Проконтроллируем, что имеем ожидаемые настройки сервера */
    @Value("#{T(java.lang.Long).parseLong(${process.simulation.time})}")
    private Long processLifeCircle;
    @LocalServerPort
    private int port;
    /** Правило для конкурентного запуска теста в нескольких потоках */
    @Rule
    public ConcurrentRule concurrentRule = new ConcurrentRule();
    /** Ожидание между запросами. Т.е. Каждый поток выполняет 20 запросов в секунду */
    private long sleepTime = 50;

    @Before
    public void setUp() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RestAssured.port = port;
    }

    @Ignore
    @Test
    // Т.е. нагрузка на сервер порядка 10.000 запросов в секунду (по 20 от каждого потока)
    @Concurrent(500)
    public void testHiLoad() {
        testWorkflow();
    }

    @Test
    @Concurrent
    public void testOk() {
        String guid = given().contentType(ContentType.ANY).
                when().
                post(ProcessService.TASK).
                then().
                statusCode(HttpStatus.ACCEPTED.value()).
                extract().body().as(String.class);

        given().contentType(ContentType.ANY).
                when().
                get(ProcessService.TASK + "/" + guid).
                then().
                statusCode(HttpStatus.OK.value()).
                extract().body();
    }

    @Test
    @Concurrent
    public void testFail() {
        // Неизвестный UUID
        UUID guid = factory.randomUUID();
        given().contentType(ContentType.ANY).
                when().
                get(ProcessService.TASK + "/" + guid).
                then().
                statusCode(HttpStatus.NOT_FOUND.value());

        // Не UUID
        given().contentType(ContentType.ANY).
                when().
                get(ProcessService.TASK + "/" + "Не UUID").
                then().
                statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void testWorkflow() {
        requestSpecification = new RequestSpecBuilder().
                setBaseUri("http://localhost").
                // setPort(8080). // random port
                        setAccept(ContentType.JSON).
                        setContentType(ContentType.ANY).
                        build();

        String guid = given().contentType(ContentType.ANY).
                when().
                post(ProcessService.TASK).
                then().
                statusCode(HttpStatus.ACCEPTED.value()).
                extract().body().as(String.class);

        responseSpecification = new ResponseSpecBuilder().
                expectStatusCode(HttpStatus.OK.value()).
                expectContentType("application/json").
                expectBody(containsString("status")).
                expectBody(containsString("timestamp")).
                build();

        if (testStatus(guid, Status.running)) {
            throw new IllegalStateException("Сервис не перевел процесс в статус " + Status.running +
                    " за " + (processLifeCircle * 2) + " сек");
        }
        if (testStatus(guid, Status.finished)) {
            throw new IllegalStateException("Сервис не перевел процесс в статус " + Status.finished +
                    " за " + (processLifeCircle * 2) + " сек");
        }
    }

    private boolean testStatus(String guid, Status status) {
        AtomicBoolean isError = new AtomicBoolean(false);
        Thread thread = new Thread(() -> {
            long time = System.currentTimeMillis();
            // В конфиге весь процесс настроен на 4 секунды
            while (true) {
                if (System.currentTimeMillis() - time > processLifeCircle * 2) {
                    isError.getAndSet(true);
                    break;
                }
                Object o = given().contentType(ContentType.ANY).
                        when().
                        get(ProcessService.TASK + "/" + guid).
                        then().
                        statusCode(HttpStatus.OK.value()).
                        extract().body().as(Object.class);
                String response = String.valueOf(o);
                if (response.contains(status.toString())) {
                    break;
                } else {
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) { isError.getAndSet(true); }
                }
            }
        });
        try {
            thread.start();
            thread.join();
            return isError.get();
        } catch (InterruptedException e) {
            return true;
        }
    }
}
