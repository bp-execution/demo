package ru.pp.bp.impl.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.Database;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultProcessFactoryTest {
    @Autowired
    private DefaultProcessFactory factory;
    @Autowired
    private Database database;

    @Test
    public void createProcess() {
        Process newProcess = factory.createProcess();
        Process dbProcess = database.retrieve(newProcess.getId());

        assertThat(newProcess).isNotNull();
        assertThat(newProcess).isEqualTo(dbProcess);
    }
}