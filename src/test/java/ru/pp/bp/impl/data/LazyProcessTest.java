package ru.pp.bp.impl.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.datasource.Database;
import ru.pp.bp.framework.service.TimeFactory;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = {"classpath:test.properties"})
public class LazyProcessTest {
    /** Время симуляции исполнения {@link Process Процессов}. По-умолчанию 2 минуты. */
    @Value("#{T(java.lang.Long).parseLong(${process.simulation.time} ?: '120000')}")
    private Long simulationTime;
    @Autowired
    private DefaultProcessFactory processFactory;
    @Autowired
    private Database database;
    @Autowired
    private TimeFactory timeFactory;

    private Process process;
    private Process lazyProcess;

    @Before
    public void setUp() {
        process = processFactory.createProcess();
        lazyProcess = new LazyProcess(process, simulationTime);
    }

    @Test
    public void testConstructor() {
        assertThat(process).isEqualTo(lazyProcess);
    }

    @Test
    public void getTime() {
        assertThat(lazyProcess.getTime().getTime()).isEqualTo(process.getTime().getTime());
        sleep();
        assertThat(lazyProcess.getTime().getTime()).isEqualTo(process.getTime().getTime() + simulationTime);
    }

    @Test
    public void getStatus() {
        assertThat(process.getStatus()).isEqualTo(Status.created);
        assertThat(lazyProcess.getStatus()).isEqualTo(Status.running);
        sleep();
        assertThat(process.getStatus()).isEqualTo(Status.created);
        assertThat(lazyProcess.getStatus()).isEqualTo(Status.finished);
    }

    private void sleep() {
        try {
            Thread.sleep(simulationTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}