package ru.pp.bp.impl.datasource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.ProcessFactory;
import ru.pp.bp.framework.datasource.Cache;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = {"classpath:test.properties"})
public class DefaultCacheTest {
    /** Максимальная емкость буфера */
    @Value("#{T(java.lang.Integer).parseInt(${cache.capacity} ?: '100000')}")
    private int capacity;
    @Autowired
    private Cache cache;
    @Autowired
    private ProcessFactory factory;

    @Test
    public void testCapacity() {
        int testCapacity = 999;
        assertThat(capacity).isEqualTo(testCapacity);

        Process firstProcess = factory.createProcess();
        Process secondProcess = factory.createProcess();
        cache.put(firstProcess);
        cache.put(secondProcess);
        assertThat(cache.retrieve(firstProcess.getId())).isEqualTo(firstProcess);

        for(int i = 0; i < testCapacity - 1; i++) {
            cache.put(factory.createProcess());
        }
        // После запроса переместится в хвост очереди и при добавлении следующего элемента не будет удален
        assertThat(cache.retrieve(firstProcess.getId())).isEqualTo(firstProcess);

        cache.put(factory.createProcess());
        // Не будет удален - элементы удаляются в порядке самого старого доступа
        assertThat(cache.retrieve(firstProcess.getId())).isEqualTo(firstProcess);
        // Окажется в голове очереди и будет удален вместо первого
        // Элементы удаляются - кэш не переполняется
        assertThat(cache.retrieve(secondProcess.getId())).isNull();
    }
}