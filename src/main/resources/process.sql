drop table IF EXISTS public.process;

create type process_status as ENUM ('created', 'running', 'finished');

CREATE TABLE public.process (
	id uuid NOT NULL,
	"time" timestamp  NOT NULL,
	"status" varchar(16) NOT NULL,
	CONSTRAINT process_pkey PRIMARY KEY (id)
);

CREATE INDEX process_status_idx ON public.process USING btree ("status");


