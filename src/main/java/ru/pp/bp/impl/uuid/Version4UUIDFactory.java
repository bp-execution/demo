package ru.pp.bp.impl.uuid;

import ru.pp.bp.framework.uuid.UUIDFactory;

import java.util.UUID;

/** Java реализация UUID версии 4 */
public class Version4UUIDFactory implements UUIDFactory {
    @Override
    public UUID randomUUID() {
        return UUID.randomUUID();
    }
}
