package ru.pp.bp.impl.uuid;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pp.bp.framework.annotation.Problem;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.uuid.UUIDFactory;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.UUID;

/**
 * Адаптер генератора UUID для Hibernate.
 * Выставляется в конфигурации к {@link Process#getId()}
 *</p>
 * Подход работает, однако при этом приходится инициализировать поле {@link #factory} вручную конкретной
 * фабрикой, т.е. нельзя использовать конфигурацию, из за которой все это и затевалось.
 * Можно было бы попробовать инициализировать данное поле в ручном режиме в {@link PostConstruct},
 * но он в данном случае тоже не работает, т.к. Hibernate инстанцирует этот класс в объод Spring.
 *</p>
 * В принципе, у Hibernate есть доступ к application.properties и hibernate.properties, где и берет
 * начало конфигурация, однако инициализировать поле таким образом совсем криво.
 *</p>
 * В результате отключаем автоматическую генерацию UUID средствами Hibernate и выставляем {@link Process#getId()}
 * вручную в конструкторе {@link Process}.
 */
@Slf4j
@Problem(context = "Hibernate не инициализирует Bean генератора ==> не удастся использовать @Configuration")
public class HibernateUUIDFactory implements UUIDFactory, IdentifierGenerator {
    @Autowired
    private UUIDFactory factory;

    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o)
            throws HibernateException {
        return randomUUID();
    }

    @Override
    public UUID randomUUID() { return factory.randomUUID(); }

    @PostConstruct
    private void info() { log.info("Initialized with " + factory); }
}
