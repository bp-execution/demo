package ru.pp.bp.impl.uuid;

import ru.pp.bp.framework.uuid.UUIDFactory;

import java.util.UUID;

/**
 * Вокэраунд с целью уменьшения падения производительности при использовании UUID в качестве первичных ключей
 * <p/>
 * Используем тот факт, что UUID версии 4, все равно, не гарантируют уникальности.
 * Поэтому, несмотря на низкую вероятность коллизий, нам все равно придется обрабатывать этот кейс.
 * Обрабатывать коллизии будем генерацией нового UUID и повторной вставкой объекта в БД.
 * А поскольку, у нас уже будет данный функционал, то мы можем воспользоваться тем фактом, что даже при значительном
 * уменьшении количества уникальных бит в UUID вероятность коллизий, все равно, останется чрезвычайно низкой.
 * <p/>
 * Наибольший вклад в overhead для INSERT в индекс вносят младшие байты. Зафиксируем их.
 * Пример, генерируемых фабрикой последовательных UUID:
 * <p/>
 * <p>29169f15-a211-4894-af0d-016ddc4cd9a8
 * <p>4e22f873-90ec-437b-8bc6-016ddc4cd9a8
 * <p>2b648e91-cd19-42b7-b172-016ddc4cd9a8
 * <p>617592b7-ddef-4942-a28e-016ddc4cd9a8
 * <p>bc611575-7d56-4285-ab84-016ddc4cd9a8
 * <p>640e08c8-8444-4ed7-bc63-016ddc4cd9a8
 * <p>a675fbd9-b140-4007-9794-016ddc4cd9a8
 * <p>10199b38-94ca-4d38-b5ff-016ddc4cd9a8
 * <p>8bea6bb0-e445-4340-9355-016ddc4cd9a8
 */
public class OptimisticUUIDFactory implements UUIDFactory {
    /** "Хвост" UUID - константа. Изменяется только после перезапуска сервиса */
    private final long tail = System.currentTimeMillis();

    @Override
    public UUID randomUUID() {
        UUID uuid = UUID.randomUUID();
        final long most = uuid.getMostSignificantBits();
        long least = uuid.getLeastSignificantBits();
        least >>= 48;
        least <<= 48;
        least += tail;
        return new UUID(most, least);
    }
}
