package ru.pp.bp.impl.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.pp.bp.framework.uuid.UUIDFactory;
import ru.pp.bp.impl.config.condition.OptimisticUUIDCondition;
import ru.pp.bp.impl.uuid.OptimisticUUIDFactory;
import ru.pp.bp.impl.config.condition.Version4UUIDCondition;
import ru.pp.bp.impl.uuid.Version4UUIDFactory;

@Slf4j
@Configuration
@PropertySource("classpath:application.properties")
public class UUIDFactoryConfiguration {

    @Bean
    @Conditional(Version4UUIDCondition.class)
    public UUIDFactory withVersion4UUIDFactory() {
        log.info("Initialized with " + UUIDFactory.Strategy.Version4);
        return new Version4UUIDFactory();
    }

    @Bean
    @Conditional(OptimisticUUIDCondition.class)
    public UUIDFactory withOptimisticUUIDFactory() {
        log.info("Initialized with " + UUIDFactory.Strategy.Optimistic);
        return new OptimisticUUIDFactory();
    }
}
