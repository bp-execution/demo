package ru.pp.bp.impl.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.pp.bp.framework.datasource.Cache;

import java.util.Arrays;

public class LazyCacheCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        final String name = context.getEnvironment().getProperty(Cache.STRATEGY);

        return Arrays.stream(Cache.Strategy.values()).filter(strategy -> strategy.name().equals(name)).
                findFirst().orElse(Cache.Strategy.No) == Cache.Strategy.No;
    }
}
