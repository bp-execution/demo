package ru.pp.bp.impl.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.pp.bp.framework.service.Engine;

public class DefaultEngineCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return Engine.Type.Default.name().equals(context.getEnvironment().getProperty(Engine.TYPE));
    }
}
