package ru.pp.bp.impl.config.tools;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@PropertySource("classpath:application.properties")
public class ThreadPoolConfiguration {
    @Value("#{T(java.lang.Integer).parseInt(${thread.pool.running.size} ?: '10')}")
    private Integer runningSize;
    @Value("#{T(java.lang.Integer).parseInt(${thread.pool.finished.size} ?: '10')}")
    private Integer finishedSize;

    @Bean
    public ThreadPoolTaskScheduler threadPool() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(runningSize + finishedSize + 1);
        scheduler.setThreadNamePrefix("Thread-Pool-");
        return scheduler;
    }
}