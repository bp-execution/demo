package ru.pp.bp.impl.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.pp.bp.framework.datasource.Cache;

public class BasicCacheCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return Cache.Strategy.Basic.name().equals(context.getEnvironment().getProperty(Cache.STRATEGY));
    }
}
