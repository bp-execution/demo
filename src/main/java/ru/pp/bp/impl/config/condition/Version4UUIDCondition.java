package ru.pp.bp.impl.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.pp.bp.framework.uuid.UUIDFactory;

public class Version4UUIDCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return UUIDFactory.Strategy.Version4.name().equals(context.getEnvironment().
                getProperty(UUIDFactory.STRATEGY));
    }
}
