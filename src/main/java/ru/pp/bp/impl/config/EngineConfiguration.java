package ru.pp.bp.impl.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.pp.bp.framework.service.Engine;
import ru.pp.bp.impl.config.condition.DefaultEngineCondition;
import ru.pp.bp.impl.config.condition.LazyEngineCondition;
import ru.pp.bp.impl.service.DefaultEngine;
import ru.pp.bp.impl.service.LazyEngine;

/** Конфигурация {@link Engine Менеджера Процессов} */
@Slf4j
@Configuration
@PropertySource("classpath:application.properties")
public class EngineConfiguration {

    @Bean
    @Conditional(LazyEngineCondition.class)
    public Engine withLazyProcessManager() {
        log.info("Initialized with " + Engine.Type.Lazy);
        return new LazyEngine();
    }

    @Bean
    @Conditional(DefaultEngineCondition.class)
    public Engine withDefaultProcessManager() {
        log.info("Initialized with " + Engine.Type.Default);
        return new DefaultEngine();
    }
}
