package ru.pp.bp.impl.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.pp.bp.framework.datasource.Cache;
import ru.pp.bp.impl.config.condition.BasicCacheCondition;
import ru.pp.bp.impl.config.condition.LazyCacheCondition;
import ru.pp.bp.impl.datasource.DefaultCache;
import ru.pp.bp.impl.datasource.LazyCache;

/** Конфигурация {@link Cache Кэша} */
@Slf4j
@Configuration
@PropertySource("classpath:application.properties")
public class CacheConfiguration {

    @Bean
    @Conditional(LazyCacheCondition.class)
    public Cache withNoCache() {
        log.info("Initialized with " + Cache.Strategy.No);
        return new LazyCache();
    }

    @Bean
    @Conditional(BasicCacheCondition.class)
    public Cache withBasicCache() {
        log.info("Initialized with " + Cache.Strategy.Basic);
        return new DefaultCache();
    }
}
