package ru.pp.bp.impl.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.pp.bp.framework.service.Engine;
import ru.pp.bp.impl.config.condition.DefaultDatabaseCondition;
import ru.pp.bp.impl.config.condition.LazyDatabaseCondition;
import ru.pp.bp.impl.datasource.DefaultDatabase;
import ru.pp.bp.impl.datasource.LazyDatabase;

@Slf4j
@Configuration
@PropertySource("classpath:application.properties")
public class DatabaseConfiguration {

    @Bean
    @Conditional(DefaultDatabaseCondition.class)
    public DefaultDatabase withDefaultDatabase() {
        log.info("Initialized with " + Engine.Type.Default);
        return new DefaultDatabase();
    }

    @Bean
    @Conditional(LazyDatabaseCondition.class)
    public LazyDatabase withLazyDatabase() {
        log.info("Initialized with " + Engine.Type.Lazy);
        return new LazyDatabase();
    }
}