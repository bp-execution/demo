package ru.pp.bp.impl.config.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.pp.bp.framework.uuid.UUIDFactory;

import java.util.Arrays;

public class OptimisticUUIDCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        final String name = context.getEnvironment().getProperty(UUIDFactory.STRATEGY);

        return Arrays.stream(UUIDFactory.Strategy.values()).filter(strategy -> strategy.name().equals(name)).
                findFirst().orElse(UUIDFactory.Strategy.Optimistic) == UUIDFactory.Strategy.Optimistic;
    }
}
