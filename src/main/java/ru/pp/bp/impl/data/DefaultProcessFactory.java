package ru.pp.bp.impl.data;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.ProcessFactory;
import ru.pp.bp.framework.data.ProcessRepository;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.service.TimeFactory;
import ru.pp.bp.framework.uuid.UUIDFactory;

/** Реализация фабрики создания {@link Process Процессов} */
@Service
public class DefaultProcessFactory implements ProcessFactory {
    @Autowired
    private UUIDFactory uuidFactory;
    @Autowired
    private TimeFactory timeFactory;
    @Autowired
    private ProcessRepository repository;

    @Override
    @Transactional
    public Process createProcess() {
        try {
            // UUID не гарантирует уникальности - всего лишь низкую вероятность коллизий.
            // В случае коллизии просто повторим создание процесса
            Process process = new Process(uuidFactory.randomUUID());
            process.setStatus(Status.created);
            process.setTime(timeFactory.currentTimestamp());
            return repository.saveAndFlush(process);
        } catch (DataIntegrityViolationException | ConstraintViolationException ex) {
            // Сначала перехватим исключение Spring, как фреймворка верхнего уровня
            // Затем - исключение Hibernate, как элемента нижележащего слоя
            return createProcess();
        }
    }
}
