package ru.pp.bp.impl.data;

import ru.pp.bp.framework.annotation.Experimetnal;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;

import java.sql.Timestamp;
import java.util.UUID;

/** Имитирует жизненный цикл процесса, базируясь на времени */
@Experimetnal(name = "Входит в (Lazy) линейку имитации работы сервиса, не отличимую от оригинала")
public class LazyProcess extends Process {
    private final long sleepTime;
    private final Process process;

    public LazyProcess(Process process, long sleepTime) {
        this.process = process;
        this.sleepTime = sleepTime;
    }

    @Override
    public void setTime(Timestamp time) { }

    @Override
    public void setStatus(Status status) { }

    @Override
    public UUID getId() { return process.getId(); }

    @Override
    public Timestamp getTime() {
        long realTime = process.getTime().getTime();
        if (System.currentTimeMillis() - realTime <= sleepTime) {
            return new Timestamp(realTime);
        } else {
            return new Timestamp(realTime + sleepTime);
        }
    }

    @Override
    public Status getStatus() {
        long realTime = process.getTime().getTime();
        if (System.currentTimeMillis() - realTime <= sleepTime) {
            return Status.running;
        } else {
            return Status.finished;
        }
    }
}
