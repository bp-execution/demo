package ru.pp.bp.impl.datasource;

import org.springframework.beans.factory.annotation.Value;
import ru.pp.bp.framework.annotation.Experimetnal;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.impl.data.LazyProcess;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Experimetnal(name = "Входит в (Lazy) линейку имитации работы сервиса, не отличимую от оригинала")
public class LazyDatabase extends BaseDatabase {
    /** Время симуляции исполнения {@link Process Процессов}. По-умолчанию 2 минуты. */
    @Value("#{T(java.lang.Long).parseLong(${process.simulation.time} ?: '120000')}")
    private Long simulationTime;

    @Override
    public Process retrieve(UUID uuid) {
        Process process = repository.findById(uuid).orElse(null);
        if (process == null) {
            return null;
        }
        return new LazyProcess(process, simulationTime);
    }

    @Override
    public List<Process> load(Status status) { return Collections.emptyList(); }
}
