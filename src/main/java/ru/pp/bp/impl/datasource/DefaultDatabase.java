package ru.pp.bp.impl.datasource;

import lombok.extern.slf4j.Slf4j;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;

import java.util.List;
import java.util.UUID;

/** Реализация слоя БД по-умолчанию */
@Slf4j
public class DefaultDatabase extends BaseDatabase {

    @Override
    public Process retrieve(UUID uuid) {
        return repository.findById(uuid).orElse(null);
    }

    /**
     * Загружает {@link Process Процессы} из БД по значению их {@link Status Статуса}.
     *
     * @param status {@link Status Статус} {@link Process Процесса}
     * @return стрим {@link Process Процессов}
     */
    @Override
    public List<Process> load(Status status) {
        return repository.getByStatus(status);
    }
}
