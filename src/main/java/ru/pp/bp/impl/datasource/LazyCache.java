package ru.pp.bp.impl.datasource;

import lombok.extern.slf4j.Slf4j;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.Cache;

import java.util.UUID;

/** Пустой кэш. Используется для стратегии без кэша. */
@Slf4j
public class LazyCache implements Cache {

    { log.info("Initialized"); }

    @Override
    public void put(Process process) { }

    @Override
    public void remove(Process process) { }

    @Override
    public Process retrieve(UUID uuid) {
        return null;
    }
}
