package ru.pp.bp.impl.datasource;

import org.springframework.beans.factory.annotation.Autowired;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.Database;
import ru.pp.bp.framework.data.ProcessRepository;

public abstract class BaseDatabase implements Database {
    @Autowired
    ProcessRepository repository;

    @Override
    public Process save(Process process) {
        return repository.saveAndFlush(process);
    }
}
