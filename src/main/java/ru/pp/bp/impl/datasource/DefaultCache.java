package ru.pp.bp.impl.datasource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import ru.pp.bp.framework.annotation.ProofOfConcept;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.Cache;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Реализация кэша на основе {@link LinkedHashMap кольцевого буфера} фиксированного размера.
 * <p/>
 * При обращении к сервису клиент сначала направляется в кэш, а затем в БД, если объект не найден в кэше.
 * На время изменения состояния объекты извлекаются из кэша и помещаются обратно уже по завершению
 * процесса изменения.
 * Таким образом, конкуренция отсутствует и клиент всегда видит "чистый" объект в кэше или в БД
 * в одном и том же состоянии. Т.е. "грязное чтение" отсутствует.
 */
@Slf4j
public class DefaultCache implements Cache {
    /** Максимальная емкость буфера */
    @Value("#{T(java.lang.Integer).parseInt(${cache.capacity} ?: '100000')}")
    private int capacity;
    /** Кольцевой буфер */
    private Map<UUID, Process> ring;
    @ProofOfConcept(idea = "Добавить стратегии синхронизации и реализовать в качестве декоратора к Cache")
    private ReentrantLock lock = new ReentrantLock(true);

    @PostConstruct
    private void init() {
        ring = new LinkedHashMap<UUID, Process>(capacity, 2f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<UUID, Process> eldest) {
                return size() > capacity;
            }
        };
        log.info("Initialized with cache.capacity=" + capacity);
    }

    @Override
    public void put(Process process) {
        lock.lock();
        try { ring.put(process.getId(), process); } finally { lock.unlock(); }
    }

    /** Удаляет объект из кэша. */
    @Override
    public void remove(Process process) {
        lock.lock();
        try { ring.remove(process.getId()); } finally { lock.unlock(); }
    }

    @Override
    public Process retrieve(UUID id) {
        lock.lock();
        try { return ring.get(id); } finally { lock.unlock(); }
    }
}
