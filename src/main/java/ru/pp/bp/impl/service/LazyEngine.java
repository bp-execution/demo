package ru.pp.bp.impl.service;

import lombok.extern.slf4j.Slf4j;
import ru.pp.bp.framework.annotation.Experimetnal;
import ru.pp.bp.framework.data.Process;

/**
 * Отметим, что возможны следующие три варианта
 * <l>
 * <li>БД эксклюзивно наша. В рамках данной конфигурации доступ в БД другими средствами, кроме
 * нашего сервиса невозможен.</li>
 *
 * <li>Сервис разделяет БД с другими сервисами. Однако, в рамках логики текущего домена доступ
 * к состоянию {@link Process} осуществляется только посредством текущего сервиса.</li>
 *
 * <li>Сервис разделяет БД с другими сервисами. В рамках логики текущего домена доступ
 * * к состоянию {@link Process} осуществляется в т.ч. и другими сервисами.</li>
 * </l>
 * <p>
 * <p/>
 * В рамках первых двух случаев, никакими средствами невозможно через API сервиса определить
 * действительную логику его работы. Да и не нужно, если он возвращает ожидаемый результат.
 * <p/>
 * Исходя из этих соображений, сделаем одну из самых "экономичных" реализаций сервиса, требующую
 * минимум памяти, и нагружающую БД и кэш меньше любой другой реализации.
 * А именно:
 * <l>
 * <li>Сервис по запросу POST /task создает запись в БД c сгенерированным GUID, текущим
 * временем и статусом “created” Возвращает клиенту код 202 и GUID задачи</li>
 * <li>Больше ничего не делает</li>
 * <li>При запросе GET /task/{id} загружает время создания записи из БД. При этом, все задачи
 * в БД имеют статус created.
 * Если с момента создания задачи прошло меньше 2-х минут,
 * возвращает клиенту running и время создания задачи + 1 сек. Если с момента создания задачи
 * прошло больше 2-х минут, возвращает finished и время создания задачи + 2 мин. 2 сек.
 * При этом, опять же, ничего не меняет в БД.
 * </li>
 * </l>
 * Данную реализацию можно тестировать в общем порядке. Она будет работать корректно и в то же
 * время быстрее, чем остальные реализации ;)
 */
@Slf4j
@Experimetnal(name = "Входит в (Lazy) линейку имитации работы сервиса, не отличимую от оригинала")
public class LazyEngine extends BaseEngine {

    @Override
    public void execute(Process process) { cache.put(process); }
}
