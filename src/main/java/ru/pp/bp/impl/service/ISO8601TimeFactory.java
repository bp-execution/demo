package ru.pp.bp.impl.service;

import org.springframework.stereotype.Service;
import ru.pp.bp.framework.service.TimeFactory;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Утилиты времени для ISO 8601:2004
 *
 * @see <a href="https://www.iso.org/standard/40874.html">ISO 8601:2004 Data elements
 * and interchange formats — Information interchange — Representation of dates and times</a>
 */
@Service
public class ISO8601TimeFactory implements TimeFactory {
    /**
     * Метод форматирует время в формат ISO 8601
     *
     * {@link DateTimeFormatter} требует знание зоны. Соответственно, нам необходим
     * {@link ZoneId}, к примеру как {@link ZoneId#systemDefault()} плюс {@link ZonedDateTime},
     * либо {@link ZoneOffset}.
     *
     * @param time - время в миллисекундах
     * @return - переданное время в формате ISO 8601
     */
    @Override
    public String toString(Timestamp time) {
        // return FORMATTER.format(Instant.ofEpochMilli(millis).atOffset(OFFSET));
        return FORMATTER.format(time.toInstant().atOffset(OFFSET));
    }

    @Override
    public long currentTime() {
        return System.currentTimeMillis();
    }

    @Override
    public Timestamp currentTimestamp() {
        return Timestamp.from(Instant.ofEpochMilli(currentTime()));
    }

    @Override
    public Date forwardOn(long millis) { return new Date(currentTime() + millis); }
}
