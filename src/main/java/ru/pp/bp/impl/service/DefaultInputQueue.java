package ru.pp.bp.impl.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.datasource.Database;

import javax.annotation.PostConstruct;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Реализация {@link ru.pp.bp.framework.service.InputQueue} на основе {@link LinkedBlockingQueue}
 * <p/>
 * Очередь служит для трансфера созданных {@link Process Процессов} от потоков из пула
 * сервера приложений к потокам пула нашего сервиса после завершения обработки клиентского
 * запроса и после возврата результата клиенту.
 * <p>
 * Кроме того, в случае падения сервиса, по любой причине, в БД могут остаться незавершенные
 * Процессы. Нам необходимо их "подхватить" и продолжить их обработку.
 * Т.к. это входная очередь, то здесь мы загружаем только Процессы в статусе
 * {@link Status#created}
 */
@Slf4j
@Component
public class DefaultInputQueue implements ru.pp.bp.framework.service.InputQueue {
    @Value("#{T(java.lang.Integer).parseInt(${queue.capacity} ?: '100000')}")
    private int capacity;
    @Autowired
    private Database database;
    private LinkedBlockingQueue<Process> queue;

    /**
     * Загружает созданные, но не запущенные {@link Process Процессы} из БД.
     * <p/>
     * В отличие от обработки процессов, находящихся на момент старта сервиса в состоянии
     * Process.Status.Running, полагаю, что здесь мы не должны сразу завершать процессы,
     * созданные более 2-х минут назад, т.к. эти процессы мы еще не успели запустить.
     */
    @PostConstruct
    private void init() {
        queue = new LinkedBlockingQueue<>(capacity);
        log.info("Start loading Processes in the '" + Status.created + "' status");
        // Для передачи в блок
        AtomicInteger counter = new AtomicInteger();

        database.load(Status.created).forEach(process -> {
            try {
                log.debug("Initial loaded " + process);
                queue.put(process);
                counter.incrementAndGet();
            } catch (InterruptedException e) {
                log.warn("Interrupted");
            }
        });
        log.info("Finish loading Processes in the '" + Status.created + "' status. " +
                counter.getAndIncrement() + " Processes loaded");
    }

    /** Блокирующий вызов */
    @Override
    public void put(Process process) throws InterruptedException {
        queue.put(process);
    }

    /** Блокирующий вызов */
    @Override
    public Process take() throws InterruptedException {
        return queue.take();
    }
}
