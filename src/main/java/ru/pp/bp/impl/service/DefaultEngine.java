package ru.pp.bp.impl.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.annotation.Transactional;
import ru.pp.bp.framework.annotation.ProofOfConcept;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;
import ru.pp.bp.framework.service.TimeFactory;

import javax.annotation.PostConstruct;

/** Движок исполнения {@link Process Процессов} по-умолчанию. */
@Slf4j
public class DefaultEngine extends BaseEngine {
    /** Время симуляции исполнения {@link Process Процессов}. По-умолчанию 2 минуты. */
    @Value("#{T(java.lang.Long).parseLong(${process.simulation.time} ?: '120000')}")
    private Long simulationTime;
    /** Размер пула потоков, выполняющих запуск {@link Process Процессов} */
    @Value("#{T(java.lang.Integer).parseInt(${thread.pool.running.size} ?: '10')}")
    private int consumersCount;
    @Autowired
    private DefaultInputQueue queue;
    @Autowired
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private TimeFactory timeFactory;

    /** {@link Process Процессы} передаются под управление менеджера непосредственно после создания. */
    @Override
    public void execute(Process process) {
        try {
            // Запрос блокирующий. Однако, очередь у нас безразмерная, а количество потоков
            // консьюмеров этой очереди сравнимо с количеством потоков в пуле входных соединений
            // и они чуть меньше нагружены - выполняют UPDATE вместо INSERT в таблицу с индексами
            queue.put(process);
        } catch (InterruptedException e) {
            // Вообще говоря, ничего страшного - Процесс будет "подхвачен" при следующем старте сервиса
            log.warn("Thread interrupted. " + process + " is not scheduled");
        }
    }

    @PostConstruct
    private void init() {
        log.info("Start with process.simulation.time=" + simulationTime);
        log.info("Start with thread.pool.running.size=" + consumersCount);
        pickUpRunningProcesses();
        start();
    }

    /**
     * Вообще говоря, наш сервис может "падать" по разным причинам. В этом случае в БД
     * останутся "запущенные" процессы, для которых в сервисе уже не будет соответствующих
     * запущенных задач. Эту ситуацию необходимо обработать.
     * <p/>
     * "Подбираем" ранее запущенные {@link Process Процессы} из БД, валидируем их состояние,
     * и, в зависимости от текущего времени, или сразу помечаем их как завершенные, или
     * создаем для них отложенные задачи симуляции.
     */
    @ProofOfConcept(idea = "Пакетирование апдейтов уже завершенных Process в одну транзакцию")
    private void pickUpRunningProcesses() {
        database.load(Status.running).forEach(process -> {
            // Проверим, возможно процесс уже должен быть завершен.
            long timeLeft = process.getTime().getTime() + simulationTime - timeFactory.currentTime();
            if (timeLeft <= 0) {
                finish(process);
            } else {
                // Если до момента завершения процесса еще остается время - запускаем процесс ожидания
                // По завершению процесса ожидания "останавливаем" процесс.
                scheduler.schedule(() -> finish(process), timeFactory.forwardOn(timeLeft));
            }
        });
    }

    /** Основная логика изменения жизненного цикла {@link Process Процессов} */
    private void start() {
        for (int i = 0; i < consumersCount; i++) {
            // Данные обработчики, в количестве consumersCount штук, забирают вновь созданные процессы
            // из очереди, переводят их в статус Running и создают для каждого из них отложенные задачи
            scheduler.execute(() -> {
                // Эти потоки мы забираем у шедулера на все время жизни сервиса. В конфигурации
                // по-умолчанию - это половина потоков пула.
                try {
                    //noinspection InfiniteLoopStatement
                    while (true) {
                        // Блокирующий вызов
                        Process process = queue.take();
                        log.debug("Take " + process);
                        // "Запускаем" процесс. Если мы здесь, то процесс еще не попадал в кэш
                        Process updated = update(process, Status.running);
                        // Добавляем процесс в кэш после "Запуска"
                        cache.put(updated);

                        // На оставшейся половине потоков запускаем задачи симуляции. Планируем "остановку" процесса
                        scheduler.schedule(() -> finish(updated), timeFactory.forwardOn(simulationTime));
                    }
                } catch (InterruptedException e) {
                    log.warn("Interrupted");
                }
            });
        }
    }

    /** Завершает {@link Process} */
    private void finish(Process process) {
        // Удаляем из кэша перед изменением
        cache.remove(process);
        // Добавляем в кэш после изменения
        cache.put(update(process, Status.finished));
    }

    @Transactional
    private Process update(Process process, Status status) {
        process.setStatus(status);
        process.setTime(timeFactory.currentTimestamp());
        return database.save(process);
    }
}
