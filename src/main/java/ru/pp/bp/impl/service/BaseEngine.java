package ru.pp.bp.impl.service;

import org.springframework.beans.factory.annotation.Autowired;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.Cache;
import ru.pp.bp.framework.datasource.Database;
import ru.pp.bp.framework.service.Engine;

import java.util.UUID;

public abstract class BaseEngine implements Engine {
    /** Слой доступа к данным. */
    @Autowired
    Database database;
    /** Кэш завершенных процессов */
    @Autowired
    Cache cache;
    /**
     * Поштучно по идентификатору Процессы выбираются только фасадом при запросе из
     * {@link DefaultProcessService#getTask(String)}
     *
     * @param uuid идентификатор {@link Process Процесса}
     * @return {@link Process Процесс} соответствующий идентификатору или <code>null</code>, если
     * {@link Process Процесс} не найден.
     */
    public Process retrieve(UUID uuid) {
        // Процесс изменяется, когда отсутствует в кэше. В это время, в случае запроса, он выбирается
        // из БД. Объект помещается в кэш в том же состоянии, в котором он находится в БД. Т.е. здесь
        // отсутствует какая либо конкуренция.
        Process process = cache.retrieve(uuid);
        if (process == null) {
            return database.retrieve(uuid);
        }
        return process;
    }
}
