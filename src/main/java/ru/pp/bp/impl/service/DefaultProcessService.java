package ru.pp.bp.impl.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.ProcessFactory;
import ru.pp.bp.framework.service.Engine;
import ru.pp.bp.framework.service.ProcessService;
import ru.pp.bp.framework.uuid.UUIDFactory;

import java.util.UUID;

/** Основная точка входа. Реализация API сервиса. */
@Slf4j
@RestController
public class DefaultProcessService implements ProcessService {
    @Autowired
    private UUIDFactory uuidFactory;
    @Autowired
    private Engine engine;
    @Autowired
    private ProcessFactory processFactory;

    /**
     * 1. POST /task
     * - Без параметров
     * - Создает запись в БД (любой) c сгенерированным GUID, текущим временем и статусом “created”
     * - Возвращает клиенту код 202 и GUID задачи
     *
     * @return в случае удачи - GUID созданного {@link Process процесса} и код 202
     */
    @Override
    @PostMapping(ProcessService.TASK)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public UUID task() {
        Process process = processFactory.createProcess();
        engine.execute(process);
        return process.getId();
    }

    /**
     * 2. GET /task/{id}
     * - Параметр id: GUID созданной задачи
     * - Возвращает код 200 и статус запрошенной задачи
     * - Возвращает 404, если такой задачи нет
     * - Возвращает 400, если передан не GUID
     *
     * @param guid {@link UUID} {@link Process Процесса} в виде строки
     * @return json вида
     * {
     * "status" : "running",                           // "created", "finished"
     * "timestamp" : "2019-10-19T13:00:24.472+03:00"   // ISO 8601
     * }
     */
    @Override
    @GetMapping(ProcessService.TASK_GUID)
    public ResponseEntity<Process> getTask(@PathVariable String guid) {
        final UUID uuid = uuidFactory.fromString(guid);
        if (uuid == null) {
            return ResponseEntity.badRequest().body(null);
        }
        Process process = engine.retrieve(uuid);
        if (process == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(process);
    }

    @GetMapping(ProcessService.ROOT)
    public String index() {
        return "BP root domain";
    }
}
