package ru.pp.bp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/** Собственно приложение. */
@Slf4j
@SpringBootApplication
//@EnableAutoConfiguration
@ComponentScan(basePackages = {"ru.pp.bp"})
public class Application {

    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
        log.warn("Started");
    }
}
