package ru.pp.bp.framework.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.pp.bp.framework.service.TimeFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

/** Объектное представление исполняемого процесса. */
@ToString
@Entity
@Data
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@Table(name = "process")
@ApiModel(description = "Executable business process object")
public class Process implements Serializable {

    /** Конструктор для JPA */
    public Process() {}

    public Process(UUID id) { this.id = id; }

    /** Идентификатор {@link Process Процесса} */
    @Id
    @Type(type = "pg-uuid")
    @ApiModelProperty(hidden = true) // Скрывает атрибут только в Swagger... использую @JsonIgnore
    @JsonIgnore
    @Column(name = "id", updatable = false, nullable = false)
    // Этот подход для нас не работает. Смотри комментарии в HibernateUUIDFactory
    // @GeneratedValue(generator = "UUID")
    // @GenericGenerator(name = "UUID", strategy = "ru.mts.bp.impl.uuid.HibernateUUIDFactory")
    private UUID id;

    /** Время изменения статуса процесса */
    @ApiModelProperty(hidden = true) // Скрывает атрибут только в Swagger... использую @JsonIgnore
    @JsonIgnore
    @Column(name = "time", nullable = false)
    private Timestamp time;

    /** Текущий {@link Status Статус} {@link Process Процесса} */
    @Type(type = "pgsql_enum")
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(name = "status", value = "Process status", position = 1, required = true)
    // create type process_status as ENUM ('created', 'running', 'finished');
    @Column(columnDefinition = "process_status", name = "status", length = 16, nullable = false)
    private Status status;

    @ApiModelProperty(name = "timestamp", value = "Process status change time", position = 2, required = true)
    public String getTimestamp() {
        return TimeFactory.timestampToString(getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof Process) {
            Process process = (Process) o;
            return Objects.equals(getId(), process.getId());
        } else {return false;}
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
