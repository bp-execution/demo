package ru.pp.bp.framework.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Имя таблицы "process"
 * суффикс "es" добавляется автоматически spring-boot-starter-data-rest
 * http://localhost:8080/processes
 * изменим это аннотацией {@link RepositoryRestResource}
 * финально
 * http://localhost:8080/repository
 */
@RepositoryRestResource(collectionResourceRel = "processes", path = "repository")
public interface ProcessRepository extends JpaRepository<Process, UUID> {

    /**
     * Выборка спика процессов по их статусу.
     * http://localhost:8080/repository/search
     * http://localhost:8080/profile/repository
     * Запрос
     * http://localhost:8080/repository/search/getByStatus?status=finished
     *
     * @param status {@link Status статус} {@link Process Процесса}
     * @return список {@link Process Процессов в переданном {@link Status статусе}}
     */
    @Transactional
    List<Process> getByStatus(@Param("status") Status status);
}
