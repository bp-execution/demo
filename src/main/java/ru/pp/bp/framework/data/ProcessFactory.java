package ru.pp.bp.framework.data;

/** Фабрика процессов */
public interface ProcessFactory {

    /** Создает персистентный {@link Process} */
    Process createProcess();
}
