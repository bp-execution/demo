package ru.pp.bp.framework.data;

/** Статус {@link Process Процесса} */
public enum Status {
    /** Присваивается {@link Process Процессу} сразу после создания. */
    created,
    /** Присваивается {@link Process Процессу} после запуска. */
    running,
    /** Присваивается {@link Process Процессу} при его завершении */
    finished;
}
