package ru.pp.bp.framework.datasource;

import ru.pp.bp.framework.data.Process;

import java.util.UUID;

/** Общий интерфейс источников данных. */
public interface DataSource {
    /**
     * Позволяет получить экземпляр {@link Process} по его идентификатору.
     *
     * @param uuid уникальный идентификатор {@link Process}
     * @return экземпляр {@link Process} или <code>null</code>, если объект не найден
     */
    Process retrieve(UUID uuid);
}
