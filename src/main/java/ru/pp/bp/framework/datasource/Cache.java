package ru.pp.bp.framework.datasource;

import org.springframework.stereotype.Service;
import ru.pp.bp.framework.data.Process;

/** Интерфейс внутреннего кэша */
@Service
public interface Cache extends DataSource {
    String STRATEGY = "cache.strategy";

    /** Стратегия кэширования */
    enum Strategy {
        /** Не кэшируем. */
        No,
        /** Стратегия с простым кэшом. Используется по умолчанию. */
        Basic
    }

    /** Метод помещения {@link Process Процесса} в кэш */
    void put(Process process);

    /** Удаляет {@link Process Процесс} из кэша. */
    void remove(Process process);
}
