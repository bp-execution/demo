package ru.pp.bp.framework.datasource;

import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.data.Status;

import java.util.List;

/** Интерфейс БД, позволяющий исполнять запросы на выборку {@link Process Процессов} по их статусу */
public interface Database extends DataSource {

    Process save(Process process);

    /**
     * Метод загрузки {@link Process Процессов}
     *
     * @param status {@link Status Статус} {@link Process Процесса}
     * @return последовательность {@link Process Процессов}
     */
    List<Process> load(Status status);
}
