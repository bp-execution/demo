package ru.pp.bp.framework.uuid;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

/** Фабрика глобально-уникальных идентификаторов. */
public interface UUIDFactory {
    String STRATEGY = "uuid.strategy";

    /** Стратегия генерации идентификаторов */
    @Slf4j
    enum Strategy {
        /** Оптимистичная. Собственная реализация. Используется по умолчанию. */
        Optimistic,
        /** Стандартная версии 4 */
        Version4
    }

    /**
     * Метод создания уникальных идентификаторов
     *
     * @return уникальный идентификатор
     */
    UUID randomUUID();

    /**
     * Создает {@link UUID} на базе его строкового представления. Не выбрасывает исключений.
     *
     * @param stringUUID строковое представление {@link UUID}
     * @return уникальный идентификатор или <code>null</code> если процесс завершился неудачей.
     */
    default UUID fromString(String stringUUID) {
        try {
            return UUID.fromString(stringUUID);
        } catch (Exception e) {
            Strategy.log.warn("Illegal UUID: '" + stringUUID + "'");
            return null;
        }
    }
}
