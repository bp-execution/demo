package ru.pp.bp.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Описывает функционал, поведение или структуру, не реализованные на текущий момент и
 * находящиеся на стадии проработки.
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.SOURCE)
public @interface ProofOfConcept {

    /** Наименование истории */
    String idea() default "";

    /** Смежные классы */
    Class<?>[] siblings() default {};
}
