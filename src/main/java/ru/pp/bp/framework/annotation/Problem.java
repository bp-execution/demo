package ru.pp.bp.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** Не решенная на данном этапе реализации, но актуальная проблема, требующая решения или описания. */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.SOURCE)
public @interface Problem {

    /** Описание истории */
    String context() default "";
}
