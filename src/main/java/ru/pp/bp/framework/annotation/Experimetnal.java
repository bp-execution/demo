package ru.pp.bp.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Экспериментальная функциональность, работоспособность которой не гарантируется.
 * Клиент сервиса использует данную фичу на свой страх и риск.
 * Может быть не документирована.
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.SOURCE)
public @interface Experimetnal {

    /** Наименование истории */
    String name() default "";
}
