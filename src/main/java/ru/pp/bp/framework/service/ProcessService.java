package ru.pp.bp.framework.service;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pp.bp.framework.data.Process;

import java.util.UUID;

/** Основная точка входа. API сервиса. */
@Api(value = "PP BP")
public interface ProcessService {
    String ROOT = "/";
    String TASK = "/task";
    String TASK_GUID = "/task/{guid}";

    /**
     * Обработчик POST /task
     * Создает запись в БД (любой) c сгенерированным GUID, текущим временем и статусом “created”
     * Возвращает клиенту код 202 и GUID задачи
     *
     * @return в случае удачи - GUID созданного {@link Process процесса} и код 202
     */
    @ApiOperation(
            value = TASK,
            httpMethod = "POST",
            response = UUID.class,
            code = 202,
            nickname = "task",
            notes = "Creation of an object emulating an executable business process"
    )
    @ApiResponse(code = 202,
            message = "",
            examples = @Example(value = {
                    @ExampleProperty(mediaType = "Response String",
                            value = "db97736c-9cc7-43f5-8783-9813d2ccc59f"
                    )}))
    @PostMapping(TASK)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    UUID task();

    /**
     * Обработчик GET /task/{id}
     * Возвращает код 200 и статус запрошенной задачи
     * Возвращает 404, если такой задачи нет
     * Возвращает 400, если передан не GUID
     *
     * @param guid {@link UUID} {@link Process Процесса} в виде строки
     * @return json вида
     * {
     * "status" : "running",                           // "created", "finished"
     * "timestamp" : "2019-10-19T13:00:24.472+03:00"   // ISO 8601
     * }
     */
    @ApiOperation(
            value = TASK_GUID,
            httpMethod = "GET",
            response = Process.class,
            nickname = "getTask",
            notes = "Getting the state of a previously created business process object"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200,
                    message = "",
                    response = Process.class,
                    examples = @Example(value = {
                            @ExampleProperty(mediaType = "Response JSON",
                                    value = "{\n" +
                                            "    \"status\" : \"running\",\n" +
                                            "    \"timestamp\" : \"2019-10-19T13:00:24.472+03:00\"\n" +
                                            "}"

                            )})),
            @ApiResponse(code = 404, message = ""),
            @ApiResponse(code = 400, message = "Invalid UUID")}
    )
    @GetMapping(TASK_GUID)
    ResponseEntity<Process> getTask(
            @ApiParam(value = "UUID", required = true, name = "guid", type = "java.lang.String")
            @PathVariable String guid
    );
}
