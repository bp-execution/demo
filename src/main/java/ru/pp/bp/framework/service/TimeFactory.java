package ru.pp.bp.framework.service;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/** Сервис работы с метками времени */
public interface TimeFactory {
    DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    ZoneOffset OFFSET = OffsetDateTime.now().getOffset();

    /**
     * Преобразует время в строку.
     *
     * @param time время в миллисекундах
     * @return строковое представление даты и времени
     */
    String toString(Timestamp time);

    /**
     * Текущее время сервиса
     *
     * @return текущее время в миллисекундах
     */
    long currentTime();

    Timestamp currentTimestamp();

    /**
     * Фабричный метод создания объектов типа {@link Date}
     *
     * @param millis сдвиг в будущее в миллисекундах
     * @return дата в будущем
     */
    Date forwardOn(long millis);

    static String timestampToString(Timestamp timestamp) {
        return FORMATTER.format(timestamp.toInstant().atOffset(OFFSET));
    }
}
