package ru.pp.bp.framework.service;

import ru.pp.bp.framework.annotation.Experimetnal;
import ru.pp.bp.framework.data.Process;
import ru.pp.bp.framework.datasource.DataSource;

import java.util.UUID;

/**
 * Интерфейс менеджера {@link Process Процессов}
 * <p>Содержит основную бизнес-логику
 */
public interface Engine extends DataSource {
    String TYPE = "process.manager.type";

    /** Тип менеджера */
    @Experimetnal(name = "Lazy в процессе реализации")
    enum Type {
        /** Реализация {@link Engine} по-умолчанию. */
        Default,
        /** Реализация заглушки, неотличимой от реального сервиса */
        Lazy
    }

    /**
     * Регистрирует {@link Process Процесс} с целью дальнейшего управления его жизненным циклом.
     *
     * @param process процесс
     */
    void execute(Process process);
}
