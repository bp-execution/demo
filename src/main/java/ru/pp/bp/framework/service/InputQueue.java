package ru.pp.bp.framework.service;

import ru.pp.bp.framework.data.Process;

/**
 * Синхронизированная неограниченная очередь.
 * <p>Развязывает потоки запроса и потоки передачи {@link Process Процессов} на исполнение.
 * Масштабирует работу с {@link Process Процессами}
 */
public interface InputQueue {

    /** Помещает {@link Process Процесс} в очередь на исполнение. Блокирующий вызов. */
    public void put(Process process) throws InterruptedException;

    /** Получает {@link Process Процесс} из очереди на исполнение. Блокирующий вызов. */
    public Process take() throws InterruptedException;
}
